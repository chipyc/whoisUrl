#!/usr/bin/env python3
import sys
from urllib.parse import urlsplit
from subprocess import call


def dom():
    domain = str(sys.argv[1])  # getting string with UR
    splited = urlsplit(domain)  # splitting url with urllib
    if splited.netloc != '':
        domain = splited.netloc  # getting xxx.yyy part from url
    domain = domain.encode("idna").decode("utf-8")  # converting to idn and deleting "b''" part from it
    print(domain)


d = str(exec('dom()'))
call(['whois', d])
